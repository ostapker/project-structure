﻿using Client.Models;
using Client.Models.ExtendedModels;
using Client.Models.SimpifiedModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectModel>> GetProjectsAsync();

        Task<ProjectModel> GetProjectByIdAsync(int id);

        Task<ProjectModel> CreateProjectAsync(ProjectCreateModel projectCreateModel);

        Task<ProjectModel> UpdateProjectAsync(ProjectUpdateModel projectUpdateModel);

        Task DeleteProjectByIdAsync(int id);

        Task<string> GetProjects_TaskCountByUser(int id);

        Task<IEnumerable<ProjectDetailedInfoModel>> GetProjectsInfo();
    }
}
