﻿using Client.Models;
using Client.Models.ExtendedModels;
using Client.Models.SimpifiedModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserModel>> GetUsersAsync();

        Task<UserModel> GetUserByIdAsync(int id);

        Task<UserModel> CreateUserAsync(UserCreateModel userCreateModel);

        Task<UserModel> UpdateUserAsync(UserUpdateModel userUpdateModel);

        Task DeleteUserByIdAsync(int id);

        Task<IEnumerable<TeamWithUsersModel>> GetUsersGroupedByTeam();

        Task<UserDetailedInfoModel> GetUserInfo(int id);
    }
}
