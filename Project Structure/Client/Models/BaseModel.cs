﻿using Newtonsoft.Json;

namespace Client.Models
{
    public abstract class BaseModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
