﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Models.SimpifiedModels
{
    public class UserCreateModel
    {
#nullable enable
        public int? TeamId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime Birthday { get; set; }
    }
}
