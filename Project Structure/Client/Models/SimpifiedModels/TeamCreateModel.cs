﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Models.SimpifiedModels
{
    public class TeamCreateModel
    {
#nullable enable
        public string? Name { get; set; }
    }
}
