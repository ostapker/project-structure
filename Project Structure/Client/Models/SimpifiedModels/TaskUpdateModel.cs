﻿using Client.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Client.Models.SimpifiedModels
{
    public class TaskUpdateModel
    {
#nullable enable
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
    }
}
