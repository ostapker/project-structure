﻿
namespace Client
{
    public static class Constants
    {
        public const string BaseUrl = "http://localhost:64460";
        public const int MaxNameLength = 45;
        public const int MinAge = 10;
        public const int Indent = 16;
    }
}
