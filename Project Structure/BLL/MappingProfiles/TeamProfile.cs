﻿using AutoMapper;
using BLL.DTOs.Team;
using DAL.Entities;

namespace BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamEntity, TeamDTO>();

            CreateMap<TeamCreateDTO, TeamEntity>();
            CreateMap<TeamUpdateDTO, TeamEntity>();
        }
    }
}
