﻿using DAL.Entities.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.UnitOfWork.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<IEnumerable<TEntity>> GetAllAsync();

        Task<TEntity> GetByIdAsync(int id);

        Task CreateAsync(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Task<bool> DeleteByIdAsync(int id);
    }
}
