﻿using BLL.UnitOfWork.Interfaces;
using DAL.Context;
using DAL.Entities.Abstract;
using System.Threading.Tasks;

namespace BLL.UnitOfWork.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly Context _context;

        public UnitOfWork(Context context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public Task SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public IRepository<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return new Repository<TEntity>(_context);
        }
    }
}
