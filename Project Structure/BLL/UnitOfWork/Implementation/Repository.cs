﻿using BLL.UnitOfWork.Interfaces;
using DAL.Context;
using DAL.Entities.Abstract;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.UnitOfWork.Implementation
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly Context _context;

        public Repository(Context context)
        {
            _context = context;
        }

        public virtual Task CreateAsync(TEntity entity)
        {
            return Task.Factory.StartNew(() =>
            {
                var list = _context.Set<TEntity>();
                entity.Id = list.Max(e => e.Id) + 1;
                list.Add(entity);
            });
        }

        public virtual Task<bool> DeleteByIdAsync(int id)
        {
            return Task.Factory.StartNew(() =>
            {
                var list = _context.Set<TEntity>();
                var entity = list.FirstOrDefault(en => en.Id == id);
                return list.Remove(entity);
            });
        }

        public virtual Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return Task<IEnumerable<TEntity>>.Factory.StartNew(() => _context.Set<TEntity>());
        }

        public virtual Task<TEntity> GetByIdAsync(int id)
        {
            return Task<TEntity>.Factory.StartNew(() => _context.Set<TEntity>().FirstOrDefault(en => en.Id == id));
        }

        public virtual Task UpdateAsync(TEntity entity)
        {
            return Task.Factory.StartNew(() =>
            {
                var list = _context.Set<TEntity>();
                var oldEntity = list.FirstOrDefault(en => en.Id == entity.Id);
                list[list.IndexOf(oldEntity)] = entity;
            });
        }
    }
}
