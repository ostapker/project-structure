﻿using BLL.DTOs.Task;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDTO>> GetAllAsync();

        Task<TaskDTO> GetByIdAsync(int id);

        Task<TaskDTO> CreateAsync(TaskCreateDTO taskDTO);

        Task<TaskDTO> UpdateAsync(TaskUpdateDTO taskDTO);

        Task DeleteByIdAsync(int id);

        Task<IEnumerable<TaskDTO>> GetTasksPerformedByUser(int Id);

        Task<IEnumerable<TaskFinishedDTO>> GetFinishedTasksByUser(int Id);

        Task<IEnumerable<UserWithTasksDTO>> GetTasksGroupedByUser();
    }
}
