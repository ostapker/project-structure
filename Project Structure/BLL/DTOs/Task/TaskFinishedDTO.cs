﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs.Task
{
    public class TaskFinishedDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
