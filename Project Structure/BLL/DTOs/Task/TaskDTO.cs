﻿using BLL.DTOs.Project;
using BLL.DTOs.User;
using DAL.Enums;
using System;

namespace BLL.DTOs.Task
{
    public class TaskDTO
    {
#nullable enable
        public int Id { get; set; }

        public int ProjectId { get; set; }
        //public ProjectDTO? Project { get; set; }

        public int PerformerId { get; set; }
        public UserDTO? Performer { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime FinishedAt { get; set; }

        public TaskState State { get; set; }
    }
}
