﻿
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs.User
{
    public class TeamWithUsersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Users { get; set; }
    }
}
