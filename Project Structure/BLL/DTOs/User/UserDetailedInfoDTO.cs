﻿using BLL.DTOs.Project;
using BLL.DTOs.Task;

namespace BLL.DTOs.User
{
    public class UserDetailedInfoDTO
    {
#nullable enable
        public UserDTO? User { get; set; }
        public ProjectDTO? LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskDTO? LongestTask { get; set; }
    }
}
