﻿
namespace BLL.DTOs.Team
{
    public class TeamUpdateDTO
    {
#nullable enable
        public int Id { get; set; }

        public string? Name { get; set; }
    }
}
