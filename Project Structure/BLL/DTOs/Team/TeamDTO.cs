﻿using System;

namespace BLL.DTOs.Team
{
    public class TeamDTO
    {
#nullable enable
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
