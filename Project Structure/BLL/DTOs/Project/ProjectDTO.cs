﻿using BLL.DTOs.Task;
using BLL.DTOs.Team;
using BLL.DTOs.User;
using System;
using System.Collections.Generic;

namespace BLL.DTOs.Project
{
    public class ProjectDTO
    {
#nullable enable
        public int Id { get; set; }

        public int AuthorId { get; set; }
        public UserDTO? Author { get; set; }

        public int TeamId { get; set; }
        public TeamDTO? Team { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime Deadline { get; set; }

        public List<TaskDTO>? Tasks { get; set; }
    }
}
