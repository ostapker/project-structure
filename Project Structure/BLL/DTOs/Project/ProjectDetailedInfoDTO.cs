﻿using BLL.DTOs.Task;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs.Project
{
    public class ProjectDetailedInfoDTO
    {
#nullable enable
        public ProjectDTO? Project { get; set; }
        public TaskDTO? LongestTask { get; set; }
        public TaskDTO? ShortestTask { get; set; }
        public int TeamMembersCount { get; set; }
    }
}
