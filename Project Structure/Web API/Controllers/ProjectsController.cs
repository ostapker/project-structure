﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTOs.Project;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProjectDTO>>> Get()
        {
            return Ok(await _projectService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetById(int id)
        {
            try
            {
                return Ok(await _projectService.GetByIdAsync(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] ProjectCreateDTO projectDTO)
        {
            try
            {
                var project = await _projectService.CreateAsync(projectDTO);
                return Created($"api/Projects/{project.Id}", project);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDTO>> Put([FromBody] ProjectUpdateDTO projectDTO)
        {
            try
            {
                return Ok(await _projectService.UpdateAsync(projectDTO));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _projectService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("Projects_TaskCount/{id}")]
        public async Task<ActionResult<Dictionary<ProjectDTO, int>>> GetProjects_TaskCountByUser(int id)
        {
            try
            {
                var res = await _projectService.GetProjects_TaskCountByUser(id);
                string json = "";
                foreach(var kvp in res)
                {
                    json += $"Project:\n{JsonConvert.SerializeObject(kvp.Key, Formatting.Indented)}\n";
                    json += $"TaskCount: {kvp.Value}\n";
                }
                return Ok(json);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("DetailedInfo")]
        public async Task<ActionResult<IEnumerable<ProjectDetailedInfoDTO>>> GetProjectsInfo()
        {
            return Ok(await _projectService.GetProjectsInfo());
        }

    }
}