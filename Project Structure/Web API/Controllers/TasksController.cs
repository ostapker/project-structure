﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTOs.Task;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TaskDTO>>> Get()
        {
            return Ok(await _taskService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetById(int id)
        {
            try
            {
                return Ok(await _taskService.GetByIdAsync(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> Post([FromBody] TaskCreateDTO taskDTO)
        {
            try
            {
                var task = await _taskService.CreateAsync(taskDTO);
                return Created($"api/Tasks/{task.Id}", task);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult<TaskDTO>> Put([FromBody] TaskUpdateDTO taskDTO)
        {
            try
            {
                return Ok(await _taskService.UpdateAsync(taskDTO));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _taskService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("performedByUser/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasksPerformedByUser(int id)
        {
            try
            {
                return Ok(await _taskService.GetTasksPerformedByUser(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("finishedByUser/{id}")]
        public async Task<ActionResult<IEnumerable<TaskFinishedDTO>>> GetFinishedTasksByUser(int id)
        {
            try
            {
                return Ok(await _taskService.GetFinishedTasksByUser(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("groupedByUser")]
        public async Task<ActionResult<IEnumerable<UserWithTasksDTO>>> GetTasksGroupedByUser()
        {
            return Ok(await _taskService.GetTasksGroupedByUser());
        }
    }
}