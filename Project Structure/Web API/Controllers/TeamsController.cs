﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTOs.Team;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TeamDTO>>> Get()
        {
            return Ok(await _teamService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetById(int id)
        {
            try
            {
                return Ok(await _teamService.GetByIdAsync(id));
            }
            catch(ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Post([FromBody] TeamCreateDTO teamDTO)
        {
            try
            {
                var team = await _teamService.CreateAsync(teamDTO);
                return Created($"api/Teams/{team.Id}", team);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult<TeamDTO>> Put([FromBody] TeamUpdateDTO teamDTO)
        {
            try
            {
                return Ok(await _teamService.UpdateAsync(teamDTO));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _teamService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}