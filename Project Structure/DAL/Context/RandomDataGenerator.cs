﻿using Bogus;
using DAL.Entities;
using DAL.Enums;
using System;
using System.Collections.Generic;


namespace DAL.Context
{
    public class RandomDataGenerator
    {
        private const int TEAM_COUNT = 10;
        private const int USER_COUNT = 50;
        private const int PROJECT_COUNT = 100;
        private const int TASK_COUNT = 200;

        public ICollection<TeamEntity> GenerateRandomTeams()
        {
            int teamId = 1;
            var teamsFake = new Faker<TeamEntity>()
                .RuleFor(pi => pi.Id, f => teamId++)
                .RuleFor(pi => pi.Name, f => f.Company.CompanyName())
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2020, 1, 1), DateTime.Now));

            return teamsFake.Generate(TEAM_COUNT);
        }

        public ICollection<UserEntity> GenerateRandomUsers(ICollection<TeamEntity> teams)
        {
            int userId = 1;
            var usersFake = new Faker<UserEntity>()
                .RuleFor(pi => pi.Id, f => userId++)
                .RuleFor(pi => pi.FirstName, f => f.Person.FirstName)
                .RuleFor(pi => pi.LastName, f => f.Person.LastName)
                .RuleFor(pi => pi.Email, f => f.Person.Email)
                .RuleFor(pi => pi.Birthday, f => f.Date.Between(new DateTime(2000, 1, 1), new DateTime(2019, 12, 31)))
                .RuleFor(pi => pi.RegisteredAt, f => f.Date.Between(new DateTime(2020, 1, 1), DateTime.Now))
                .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id);

            return usersFake.Generate(USER_COUNT);
        }

        public ICollection<ProjectEntity> GenerateRandomProjects(ICollection<UserEntity> users, ICollection<TeamEntity> teams)
        {
            int projectId = 1;
            var projectsFake = new Faker<ProjectEntity>()
                .RuleFor(pi => pi.Id, f => projectId++)
                .RuleFor(pi => pi.Name, f => f.Lorem.Sentence())
                .RuleFor(pi => pi.Description, f => f.Lorem.Sentences())
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2020, 1, 1), DateTime.Now))
                .RuleFor(pi => pi.Deadline, f => f.Date.Future(2, DateTime.Now))
                .RuleFor(pi => pi.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id);

            return projectsFake.Generate(PROJECT_COUNT);
        }

        public ICollection<TaskEntity> GenerateRandomTasks(ICollection<UserEntity> users, ICollection<ProjectEntity> projects)
        {
            int taskId = 1;
            var tasksFake = new Faker<TaskEntity>()
                .RuleFor(pi => pi.Id, f => taskId++)
                .RuleFor(pi => pi.Name, f => f.Lorem.Sentence())
                .RuleFor(pi => pi.Description, f => f.Lorem.Sentences())
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2020, 1, 1), DateTime.Now))
                .RuleFor(pi => pi.FinishedAt, f => f.Date.Future(2, DateTime.Now))
                .RuleFor(pi => pi.State, f => f.Random.Enum<TaskState>())
                .RuleFor(pi => pi.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(pi => pi.PerformerId, f => f.PickRandom(users).Id);

            return tasksFake.Generate(TASK_COUNT);
        }
    }
}