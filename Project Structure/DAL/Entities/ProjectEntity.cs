﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public sealed class ProjectEntity : BaseEntity
    {
        ProjectEntity()
        {
            Tasks = new List<TaskEntity>();
        }

#nullable enable
        public int AuthorId { get; set; }

        public UserEntity? Author { get; set; }

        public int TeamId { get; set; }

        public TeamEntity? Team { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime Deadline { get; set; }

        public List<TaskEntity> Tasks { get; private set; }
    }
}
